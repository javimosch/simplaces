const opencage = require("opencage-api-client");

module.exports = {
  forwardGeocoding(query) {
    return new Promise((resolve, reject) => {
      opencage
        .geocode({ q: query, key: process.env.OPENCAGE_API_KEY })
        .then((data) => {
          data = data.results.map(result =>{
              return {
                  formatted: result.formatted,
                  street: result.components.road,
                  suburb: result.components.suburb,
                  city: result.components.city,
                  zip: result.components.postcode,
                  department: result.components.county,
                  region: result.components.state,
                  country: result.components.country,
                  country_code: result.components.country_code,
                  lat: result.geometry.lat,
                  lng: result.geometry.lng
              }
          })
          resolve(data)
        })
        .catch((error) => {
          //Send error to sentry
          console.error("FORWARD_GEOCODING_ERROR", {
            stack: error.stack,
          });
          reject(new Error("FORWARD_GEOCODING_ERROR"));
        });
    });
  },
};
