const providers = {
    opencage: require('./providers/opencage')
}
let currentProvider = process.env.GEOCODING_PROVIDER || 'opencage'

module.exports = {
    forwardGeocoding(query){
        return providers[currentProvider].forwardGeocoding(query)
    }
}