# Simplaces

Geocoding API (Reverse/Forward) for Simpliciti projects.

## Features

- API Request authenticate with APIV2
- Multiple providers (Opencage, Google Maps, OpenStreetMap Nominatim, etc)

## Endpoints

### Forward geocoding

```js
api/
```

## Installation

```sh
# node version >= 14.17.1
# Install yarn if missing
npm install -g yarn
# In the project root (same as npm install)
yarn
```

## Development

```sh
yarn dev
```